/* classie */

( function( window ) {

'use strict';

// class helper functions from bonzo https://github.com/ded/bonzo

function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}

// classList support for class management
var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
}
else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}

var classie = {
  // full names
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  // short names
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

// browser global
window.classie = classie;

})( window );



/* spinning ball */

var C = document.createElement('canvas');
var c = C.getContext('2d');

C.width = 200; C.height = 200;
document.getElementById("spinning-ball").appendChild(C);

var particles = [];
for(var i = 3000; i--; particles.push([
Math.random() * Math.PI * 1,
Math.random() * Math.PI * 1,
[
Math.random() * 256 | 0,
Math.random() * 256 | 0,
Math.random() * 256 | 0
]
]));

(function render() {
c.setTransform(1, 0, 0, 1, 0, 0);
c.scale(C.width/2, C.height/2);
c.translate(1, 1);

c.globalCompositeOperation = 'source-over';
c.fillStyle = 'rgba(0, 0, 0, .5)';
c.fillRect(-12, -2, 20, 4);

var x, y, z, w, t, p, r = .7, f = 1;
c.globalCompositeOperation = 'lighter';

for(var i = 0; p = particles[i++];) {
t = Date.now() / 8000 * (i%1 ? 1 : -1);
x = r * Math.sin(p[0] + t) * Math.cos(p[1] + t);
y = r * Math.cos(p[0] + t);
z = r * Math.sin(p[0] + t) * Math.sin(p[1] + t);
w = f / (f - z);
c.beginPath(); c.arc(x*w, y*w, w/100, 0, 2*Math.PI);
c.fillStyle = 'rgba(' + p[2] + ', ' + w/4 + ')';
c.fill();
}

requestAnimationFrame(render);
})();




/* svg slide menu */

function SVGMenu( el, options ) {
  this.el = el;
  this.init();
}

SVGMenu.prototype.init = function() {
  this.trigger = this.el.querySelector( 'button.slide-menu-handle' );
  this.shapeEl = this.el.querySelector( 'div.morph-shape' );

  var s = Snap( this.shapeEl.querySelector( 'svg' ) );
  this.pathEl = s.select( 'path' );
  this.paths = {
    reset : this.pathEl.attr( 'd' ),
    open : this.shapeEl.getAttribute( 'data-morph-open' ),
    close : this.shapeEl.getAttribute( 'data-morph-close' )
  };

  this.isOpen = false;

  this.trigger.addEventListener( 'click', this.toggle.bind(this) );
};


SVGMenu.prototype.toggle = function() {
  var self = this;

  if( this.isOpen ) {
    classie.remove( self.el, 'menu--anim' );
    setTimeout( function() { classie.remove( self.el, 'slide-menu-open' ); }, 250 );
  }
  else {
    classie.add( self.el, 'menu--anim' );
    setTimeout( function() { classie.add( self.el, 'slide-menu-open' );  }, 250 );
  }
  self.pathEl.stop().animate( { 'path' : self.isOpen ? self.paths.close : this.paths.open }, 350, mina.easeout, function() {
    self.pathEl.stop().animate( { 'path' : self.paths.reset }, 800, mina.elastic );
  } );
  
  self.isOpen = !self.isOpen;
};

new SVGMenu( document.getElementById( 'slide-menu' ) );


/* create sound cloud player */

var vm = new Vue({ el: '#vm' })
